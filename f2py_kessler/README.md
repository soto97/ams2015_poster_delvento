Example of f2py usage to call the kessler scheme written in Fortran. The WRF module `module_mp_kessler.f90` to solve problems with converting variables types.

    $ f2py -c module_mp_kessler.f90 -m module_mp_kessler

    $ python kessler_f2py_call.py