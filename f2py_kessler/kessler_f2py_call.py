""" simple example of kessler-python function usage"""

import sys
sys.path.append(".")
try:
    import numpypy as np
    from _numpypy.pypy import set_invalidation
    set_invalidation(False)
except ImportError:
    pass
import numpy as np
#from cffi_kessler import kessler

from constants_kessler import xlv, cp, EP2, SVP1, SVP2, SVP3, SVPT0, rhowater
import module_mp_kessler as mk

nx = 1
ny = 1
nz = 1
dt_in = 1.

[ims, ime, ids, ide, its, ite] = [1, nx] * 3
[jms, jme, jds, jde, jts, jte] = [1, ny] * 3
[kms, kme, kds, kde, kts, kte] = [1, nz] * 3


t_np = np.ones((nx,nz,ny), order='F') * 291.8
qv_np = np.ones((nx,nz,ny), order='F') * 10.e-3
qc_np = np.ones((nx,nz,ny), order='F') * 0.e-3
qr_np = np.zeros((nx,nz,ny), order='F')
rho_np = np.ones((nx,nz,ny), order='F')
pii_np = np.ones((nx,nz,ny), order='F') * .97
dz8w_np = np.ones((nx,nz,ny), order='F') * 20.
z_np = np.ones((nx,nz,ny), order='F') * 700.
rainnc_np = np.zeros((nx,ny), order='F')
rainncv_np = np.zeros((nx,ny), order='F')


print "in kessler_f2py_call, before calling", qv_np, qc_np
mk.module_mp_kessler.kessler(t_np, qv_np, qc_np, qr_np, rho_np, pii_np
                  ,dt_in, z_np, xlv, cp
                  ,EP2,SVP1,SVP2,SVP3,SVPT0,rhowater
                  ,dz8w_np
                  ,rainnc_np, rainncv_np
                  ,ims, ime, ids, ide, its, ite
                  ,jms, jme, jds, jde, jts, jte
                  ,kms, kme, kds, kde, kts, kte
                             )


print "in kessler_f2py_call, after calling ", qv_np, qc_np, qr_np
