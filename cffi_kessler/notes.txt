* Calling Fortran code from Python

  - allows to reuse existing Fortan code and write any new code in Python

  - allows to reuse existing Pyton and Fortran codes at the same time

  - allows using Python testing framework for Fortran code

* CFFI - C Foreign Function Interface for Python

  - provides a  way to call compiled C and Fortran code from Python 

  - keeps all the Python-related logic in Python, no need to write much C code

  - attempts to support both PyPy and CPython


* using CFFI to call a Fortran code

  - C bindings for Fortran function

    use iso_c_binding, only: c_int, c_double

     subroutine c_function_from_Fortran(arg) bind(c)
         real(c_double), intent(in), value :: arg 
         call function_from_Fortran(arg)
     end subroutine c_function_from_Fortran


  - signature for the C function in python

    from cffi import FFI
    ffi = FFI()
    ffi.cdef("void c_function_from_Fortran(double arg);")
 
  - calling the C-binding of the Fortran function

    lib = ffi.dlopen('library.so')
    lib.c_function_from_function(arg)

  - creating cdata variables of a type "double *" if we	want to	pass an	array

    arg_array = ffi.cast("double*", arg_numpy_array.__array_interface__['data'][0])

