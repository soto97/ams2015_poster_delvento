If you've made a share library - `libkessler.so` in cffi_kessler directory, you can link the library:
    
    $ ln -s ../cffi_kessler/libkessler.so

In `wrfkessler_blk_1m_pytest.py` a function calling the kessler scheme from WRF is defined, the python binding from `cffi_kessler` directory is used.
  
Python tests using pytest are defined in `blk_1m_pytest.py` (with `pytest_generate_tests` defined in `conftest.py`). Example of calling the tests for the WRF kessler library:

    $ python -m pytest -s blk_1m_pytest.py --libname=wrfkessler_blk_1m_pytest  
