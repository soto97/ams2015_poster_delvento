import numpy as np
# use pytest library for testing
import pytest

# function takes the initial values of pressure, temperature and mixing ratios
# returns water vapour and cloud water mixing ratios after condensation/evaporation
def condensation(lib, rv, rc, dt = 1, press = np.array([900.e2]), 
                 T = np.array([283.15]), rr = np.array([0.])):
    # import library for testing, lib is specified during calling
    import importlib
    lib_adj = importlib.import_module(lib)
    # calling a function from chosen library
    rv, rc, rr = lib_adj.adj_cellwise(press, T, rv, rc, rr, dt)
    return rv, rc

# check if the function returns values close to expected values
# various sets of arguments are tested 
@pytest.mark.parametrize("arg, expected", [
                  # no cloud water and supersaturation
                  ({"rv" : np.array([10.e-3]),  "rc" : np.array([0.])},
                   {"rv" : np.array([9.44e-3]), "rc" : np.array([.56e-3])}), 
                  # subsaturation leads to some evaporation          
                  ({"rv" : np.array([8.e-3]),   "rc" : np.array([1.e-3])},
                   {"rv" : np.array([8.26e-3]), "rc" : np.array([0.74e-3])}), 
                  # supersaturation leads to condensation
pytest.mark.xfail(({"rv" : np.array([9.e-3]),   "rc" : np.array([1.e-3])},
                   {"rv" : np.array([8.85e-3]), "rc" : np.array([1.15e-3])})), 
                  ])
def test_expected_output_evapcond(libname, arg, expected, epsilon = 0.1):
    rv, rc = condensation(lib=libname, **arg)
    for key, value in expected.items():
        assert abs(eval(key) - value) <= epsilon * abs(value)

